console.log("Hello, World!");

let firstName = "John";
console.log("First Name: " + firstName);
let lastName = "Last Name: Smith";
console.log(lastName);
let age = 30;
console.log("Age " + age);
console.log("Hobbies:");
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log(hobbies);
console.log("Work Address:");
let workAdress = {
	houseNumber : '32',
	street : 'Washington',
	city : 'Lincoln',
	state : 'Nebraska'
}
console.log(workAdress)

let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let stageAge = 40;
	console.log("My current age is: " + stageAge);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username : "captain_america",
		fullName : "Steve Rogers",
		age : 40,
		isActive : false

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let friendName = "Bucky Barnes";
	console.log("My bestfriend is: " + friendName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);