const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Products = require("../models/Products");


// registration
module.exports.registration = (req, res) => {
  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    mobileNo: req.body.mobileNo,
    password: bcrypt.hashSync(req.body.password, 10),
  });

  User.findOne({ email: req.body.email })
    .then((result) => {
      if (result) {
        return res.json({ emailExists: true });
      } else {
        newUser
          .save()
          .then((user) => {
            console.log(newUser);
            res.json({ registrationSuccess: true });
          })
          .catch((err) => {
            res.status(500).json({ error: "Registration failed." });
          });
      }
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
};



// authentication/login
module.exports.login = (req, res) => {
	return User.findOne({email: req.body.email})
	.then(result => {
		if(!result){
			return res.send("Please register first.")
		}else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(result)})
			}else {
				return res.send("Incorrect password please try again.")
			}
		}
	})
};

// Retrieve user details
module.exports.userDetails = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (!userData) {
    return res.status(401).send("Invalid authorization token");
  }

  User.findById(userData.id)
    .then((result) => {
      if (!result) {
        return res.status(404).send("User not found");
      }
      result.password = "";
      return res.send(result);
    })
    .catch((err) => res.status(500).send(err));
};

// Set user role as admin
module.exports.setAdmin = (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	const userId = req.params.userId
	if(userData.isAdmin){
		return User.findById(userId)
		.then(result => {
			const update = {isAdmin: !result.Admin}
			return User.findByIdAndUpdate(userId, update, {new: true})
			.then(document => {
				document.password = ""
				return res.send(document)
			})
			.catch(err => {res.send(err)})
		})
		.catch(err => {res.send(err)})
	}else{
		return res.send("Attempt failed, make sure you are logged as Admin.")
	}
};

// Create order or add to cart
module.exports.order = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const productId = req.body.productId;
  const { quantity } = req.body;
  const { productName } = req.body;
  if (!userData.isAdmin) {
    const product = await Products.findById(productId);
    if (!product) {
      return res.status(404).send("Product not found.");
    }
    let totalAmount = quantity * product.productPrice;
    const user = await User.findById(userData.id);

    if (!user.orderedProduct || user.orderedProduct.length === 0) {
      user.orderedProduct.push({
        products: [],
        totalAmount: 0,
        purchasedOn: new Date(),
      });
    }

    user.orderedProduct[0].products.push({
      productId,
      quantity
    });

    user.orderedProduct[0].totalAmount += totalAmount;
    await user.save();

    product.userOrders.push({ userId: userData.id });
    await product.save();

    return res.send(user);
  } else {
    res.send("You are an admin.");
  }
};

// Retrieve authenticated user's order
module.exports.getUserOrder = (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;

  User.findById(userId)
    .then((user) => {
      if (!user) {
        return res.status(404).send("User not found.");
      }

      if (!user.orderedProduct || user.orderedProduct.length === 0) {
        return res.status(404).send("No orders found for the user.");
      }

      const userOrder = user.orderedProduct[0]; // Assuming there's only one order per user

      return res.send(userOrder);
    })
    .catch((err) => {
      res.status(500).send("An error occurred while retrieving the user's order.");
    });
};

// Retrieve all orders by admin
module.exports.getAllOrders = (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  User.findById(userId)
    .then((user) => {
      if (!user.isAdmin) {
        return res.status(403).send("Access denied. You are not an admin.");
      }

      User.find({ isAdmin: false }) // Filter out admin users
        .then((users) => {
          const orders = [];
          users.forEach((user) => {
            if (user.orderedProduct && user.orderedProduct.length > 0) {
              orders.push(user.orderedProduct[0]); // Assuming one order per user
            }
          });

          res.send(orders);
        })
        .catch((err) => {
          res.status(500).send("An error occurred while retrieving the orders.");
        });
    })
    .catch((err) => {
      res.status(500).send("An error occurred while retrieving user information.");
    });
};




