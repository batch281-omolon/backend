const Products = require("../models/Products");
const auth = require("../auth");

// Add/create a product
module.exports.createProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	const newProduct = new Products ({
		productName: req.body.productName,
		productDescription: req.body.productDescription,
		productPrice: req.body.productPrice,
		image: req.body.image
	})
	if(userData.isAdmin){
		newProduct.save()
		.then(result => {
			// console.log(result)
			res.send(result)
		})
		.catch(err => {
			// console.log(err)
			res.send("Error, adding product failed.")
		})
	}else{
		res.send("Attempt failed, be sure you are logged as Admin.")
	}
};

// Retrieving all products
module.exports.allProducts = (req, res) => {
  Products.find({})
    .then((result) => res.json(result))
    .catch((err) => res.status(500).json({ error: err }));
};

// Retrieving active products
module.exports.activeProducts = (req, res) => {
	return Products.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

// Retrieving a single/specific product
module.exports.getProduct = (req, res) => {
	const productId = req.params.productId;
	return Products.findById(productId)
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

// Updating a product
module.exports.updateProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	const update = {
		productName: req.body.productName,
		productDescription: req.body.productDescription,
		productPrice: req.body.productPrice,
		image: req.body.image
	}
	const productId = req.params.productId;
	if(userData.isAdmin){
		return Products.findByIdAndUpdate(productId, update, {new:true})
		.then(result => res.send(result))
		.catch(err => res.send(err))
	}else {res.send ("Attempt failed, be sure you are logged as Admin.")}
};

// Archive a product
module.exports.archiveProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	const archive = {isActive: req.body.isActive}
	const productId = req.params.productId
	if(userData.isAdmin){
		return Products.findByIdAndUpdate(productId, archive, {new:true})
		.then(result => res.send(true))
		.catch(err => res.send(false))
	}else{
		res.send("Attempt failed, be sure you are logged as Admin.")
	}
};

// Activate a product
module.exports.activateProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	const activate = {isActive: req.body.isActive}
	const productId = req.params.productId
	if(userData.isAdmin){
		return Products.findByIdAndUpdate(productId, activate, {new:true})
		.then(result => res.send(true))
		.catch(err => res.send(false))
	}else{
		res.send("Attempt failed, be sure you are logged as Admin.")
	}
};