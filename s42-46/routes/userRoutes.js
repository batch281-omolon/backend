const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

// Route for registration
router.post("/register", userControllers.registration);

// Route for authentication/login
router.post("/login", userControllers.login);

// Route for retrieving user details
router.get("/details", userControllers.userDetails);

// Route for setting role as admin
router.patch("/setAdmin/:userId", auth.verify, userControllers.setAdmin);

// Route for creating order/add to cart
router.post("/order", auth.verify, userControllers.order);

// Route for retrieving authenticated user's order
router.get("/retrieveOrder", auth.verify, userControllers.getUserOrder);

// Route for retrieving all orders by admin
router.get("/orders", auth.verify, userControllers.getAllOrders);


module.exports = router;