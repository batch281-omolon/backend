const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Route for creating product
router.post("/createProduct", auth.verify, productControllers.createProduct);

// Route for retrieving all products
router.get("/all", productControllers.allProducts);

// Route for retrieving all active products
router.get("/active", productControllers.activeProducts);

// Route for retrieving single product
router.get("/:productId", productControllers.getProduct);

// Route for updating product information
router.put("/update/:productId", auth.verify, productControllers.updateProduct);

// Route for archiving a product
router.patch("/archives/:productId", auth.verify, productControllers.archiveProduct);

// Route for activating a product
router.patch("/activate/:productId", auth.verify, productControllers.activateProduct);


module.exports = router;