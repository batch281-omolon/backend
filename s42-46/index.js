// dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const app = express();

// database connection
mongoose.connect("mongodb+srv://omolonpaul:Rhx01sBDdoUFqvRW@wdc028-course-booking.nawzws6.mongodb.net/capstone2API?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// database terminal prompt
mongoose.connection.on("error", ()=>console.error.bind(console, "MongoDB connecion Error."));
mongoose.connection.once('open', ()=>console.log('Now Conencted to MongoDB Atlas!'));

// middlewares
app.use(cors());
// app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);


// listen
app.listen(process.env.PORT || 4000,()=>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})