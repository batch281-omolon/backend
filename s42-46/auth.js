const jwt = require("jsonwebtoken");
const secret = "capstone2API"


// Create token
module.exports.createAccessToken = (result) => {
	const data = {
		id : result._id,
		email: result.email,
		isAdmin : result.isAdmin
	};

	return jwt.sign(data, secret, {});
}

// Verify Token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if (typeof token !== "undefined"){
		console.log(token);
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth : "failed"})
			} else {
				next();
			}
		})
	} else {
		return res.send({auth : "failed"});
	};
};

// decode token
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			} else {
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	} else {
		return null
	};
};