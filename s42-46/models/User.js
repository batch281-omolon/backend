const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {type: String, required: [true, "First Name is required."]},
	lastName: {type: String, required : [true, "Last Name is required."]},
	email : {type: String, required: [true, "Email is required"]},
	mobileNo : {type: String, required : [true, "Mobile Number is required."]},
	password : {type: String, required: [true, "Password is required"]},
	isAdmin : {type: Boolean, default: false},
	orderedProduct : [{
		products : [{
			productId : {type: String, required: true},
			quantity : {type: Number, required: [true, "Quantity is required"]}
		}],
		
		totalAmount : {type: Number, default: 0},

		purchasedOn : {type: Date, default: new Date()}
	}]

})

module.exports = mongoose.model("User", userSchema);