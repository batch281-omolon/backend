const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName : {type: String, required: [true, "Product is required"]},
	productDescription : {type: String, required: [true, "Description is required"]},
	productPrice : {type: Number, required: [true, "{Price is required"]},
	image: {type: String, required: true},
	isActive : {type: Boolean, default: true},
	createdOn : {type: Date, default: new Date()},
	userOrders : [{
		userId : {type: String}
		// orderId : {type: String}
	}]
});

module.exports = mongoose.model("Products", productSchema);